import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Contact from './views/Contact.vue'
import ExploreHotels from './views/ExploreHotels.vue'
import Booking from './views/Booking.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/explorehotels',
      name: 'explorehotels',
      component: ExploreHotels,
    },
    {
      path: '/booking',
      name: 'booking',
      component: Booking
    },
      
  ]
})
