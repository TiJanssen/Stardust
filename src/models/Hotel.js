import AvailableFacility from './AvailableFacility.js';
import Room from './Room.js';
import Review from './Review.js';

export default class Hotel {
	constructor() {
		this.id = 0;
		this.name = '';
		this.stars = '';
		this.address = '';
		this.description = '';
		this.image = '';
		this.availableFacilities = [];
		this.rooms = [];
		this.reviews = [];
	}

	static fromJSON(json) {
		const self = new Hotel();

		self.id = json.id;
		self.name = json.name;
		self.stars = json.stars;
		self.address = json.address;
		self.description = json.description;
		self.image = json.image;
		self.availableFacilities = json.availableFacilities.map(AvailableFacility.fromJSON);
		self.rooms = json.rooms.map(Room.fromJSON);
		self.reviews = json.reviews.map(Review.fromJSON);

		return self;
	}
}