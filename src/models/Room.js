export default class Room {
	constructor() {
		this.price = '';
	}

	static fromJSON(json) {
		const self = new Room();

		self.price = json.price;

		return self;
	}
}